<?php

namespace Test\Geo\Model;

use Geo\Model\ClientLocation;
use PHPUnit\Framework\TestCase;

class ClientLocationTest extends TestCase
{
    /**
     * @covers \Geo\Model\ClientLocation::isValid()
     */
    public function testIsValid()
    {
        $sut = new ClientLocation;

        $this->assertFalse($sut->isValid(false));
        $this->assertFalse($sut->isValid(0));
        $this->assertFalse($sut->isValid(10));

        $sut->ip = 2996208015;
        $this->assertFalse($sut->isValid(5));

        $sut->updated_at = new \DateTime;
        $this->assertTrue($sut->isValid(7));
        $this->assertTrue($sut->isValid(false));
        $this->assertTrue($sut->isValid(0));

        $sut->updated_at->modify('-60 second');
        $this->assertFalse($sut->isValid(60));
        $this->assertTrue($sut->isValid(70));
    }

}
