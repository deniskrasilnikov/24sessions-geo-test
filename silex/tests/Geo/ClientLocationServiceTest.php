<?php

namespace Test\Geo;

use Geo\ClientLocationService;
use Geo\LocationInfo;
use Geo\LocationProviderInterface;
use Geo\Model\ClientLocation;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class ClientLocationServiceTest extends TestCase
{
    private $httpClient;
    private $locationProvider;
    private $logger;
    private $request;

    /**
     * @covers \Geo\ClientLocationService::getClientIp()
     */
    public function testGetClientIp()
    {
        $this->request->server = new ParameterBag(['REMOTE_ADDR' => '7.22.1.103']);

        $sut = new ClientLocationService(
            $this->httpClient,
            $this->request,
            $this->locationProvider,
            $this->logger
        );

        $this->assertEquals('7.22.1.103', $sut->getClientIp());

        $this->request->server->set('REMOTE_ADDR', '234.31.3.4.');
        $this->assertNull($sut->getClientIp());
    }

    /**
     * @covers \Geo\ClientLocationService::getClientIp()
     */
    public function testGetClientIpInDevMode()
    {
        $response = new Response(200, [], 'Your IP Address: 167.150.42.143');

        $this->httpClient
            ->method('request')
            ->willReturn($response);

        $sut = new ClientLocationService(
            $this->httpClient,
            $this->request,
            $this->locationProvider,
            $this->logger,
            true
        );

        $this->assertEquals('167.150.42.143', $sut->getClientIp());
    }

    /**
     * @covers \Geo\ClientLocationService::getLocationInfo()
     */
    public function testGetLocationInfoWithNewIpAddress()
    {
        $sut = $this->getMockBuilder(ClientLocationService::class)
            ->setConstructorArgs([
                $this->httpClient,
                $this->request,
                $this->locationProvider,
                $this->logger,
                false,
                3600
            ])
            ->setMethods(['getClientIp', 'createLocation'])
            ->getMock();

        $sut->expects($this->once())
            ->method('getClientIp')
            ->willReturn('167.150.42.143');

        $location = $this->createMock(ClientLocation::class);
        $location->expects($this->once())->method('isValid')->with($this->equalTo(3600));
        $location->expects($this->once())->method('save');
        $location->method('to_array')->willReturn(['data']);
        $location->errors = new \ActiveRecord\Errors;

        $sut->expects($this->once())
            ->method('createLocation')
            ->willReturn($location);

        $this->locationProvider
            ->expects($this->once())
            ->method('getLocationInfo')
            ->with($this->equalTo('167.150.42.143'))
            ->willReturn(new LocationInfo('defaultCity', 'DC'));

        $this->assertEquals(['data'], $sut->getLocationInfo());
    }

    protected function setUp()
    {
        $this->httpClient = $this->createMock(ClientInterface::class);
        $this->request = $this->createMock(Request::class);
        $this->locationProvider = $this->createMock(LocationProviderInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }
}
