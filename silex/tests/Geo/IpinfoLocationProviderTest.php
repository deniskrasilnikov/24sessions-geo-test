<?php

namespace Test\Geo;

use Geo\IpinfoLocationProvider;
use Geo\LocationInfo;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class IpinfoLocationProviderTest extends TestCase
{
    /**
     * @covers IpinfoLocationProvider::getLocationInfo()
     */
    public function testGetLocationInfo()
    {
        $clientIp = '11.22.33.44';
        $apiToken = '3X#@fv40!72';
        $response = new Response(200, [], '{"country":"UA","city":"Kharkiv"}');

        $httpClientMock = $this->createMock(ClientInterface::class);
        $httpClientMock->expects($this->atLeastOnce())
            ->method('request')
            ->with(
                $this->equalTo('GET'),
                $this->equalTo("http://ipinfo.io/$clientIp/geo?token=$apiToken")
            )
            ->willReturn($response);

        $sut = new IpinfoLocationProvider($httpClientMock, $apiToken);
        $this->assertInstanceOf(LocationInfo::class, $info = $sut->getLocationInfo($clientIp));
        $this->assertEquals('Ukraine', $info->getCountry());
        $this->assertEquals('Kharkiv', $info->getCity());
    }

    /**
     * @covers IpinfoLocationProvider::getLocationInfo()
     * @expectedException \Geo\Exception\LocationProviderException
     * @expectedExceptionMessage Error on requesting geo location info
     */
    public function testGetLocationInfoThrowsExceptionOnRequestIssue()
    {
        $httpClientMock = new Client;
        $sut = new IpinfoLocationProvider($httpClientMock, '');
        $sut->getLocationInfo('');
    }

    /**
     * @covers IpinfoLocationProvider::getLocationInfo()
     * @expectedException \Geo\Exception\LocationProviderException
     * @expectedExceptionMessage Bad response (code 909) from geo location service: error text
     */
    public function testGetLocationInfoThrowsExceptionOnBadResponse()
    {
        $response = new Response(909, [], 'error text');

        $httpClientMock = $this->createMock(ClientInterface::class);
        $httpClientMock->method('request')->willReturn($response);

        $sut = new IpinfoLocationProvider($httpClientMock, '');
        $sut->getLocationInfo('');
    }

    /**
     * @covers IpinfoLocationProvider::getLocationInfo()
     * @expectedException \Geo\Exception\LocationProviderException
     * @expectedExceptionMessage Error on getting valid json response: invalid json
     */
    public function testGetLocationInfoThrowsExceptionOnInvalidResponseJson()
    {
        $response = new Response(200, [], 'invalid json');

        $httpClientMock = $this->createMock(ClientInterface::class);
        $httpClientMock->method('request')->willReturn($response);

        $sut = new IpinfoLocationProvider($httpClientMock, '');
        $sut->getLocationInfo('');
    }
}
