<?php

use ActiveRecord\Config;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Stubs/ActiveRecord/Errors.php';

Config::initialize(function (Config $cfg) {
    $cfg->set_connections([
        'test' => sprintf('mysql://%s:%s@db/%s', getenv('DB_USER'), getenv('DB_PASSWORD'), getenv('DB_NAME'))
    ]);
    $cfg->set_default_connection('test');
});
