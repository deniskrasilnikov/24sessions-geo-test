<?php

namespace Geo;

use Geo\Exception\ClientLocationException;
use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Geo\ClientLocationService as GeoClient;

class ControllerProvider implements ControllerProviderInterface
{
    /**
     * {@inheritdoc}
     *
     * @param Application $app
     *
     * @return mixed|\Silex\ControllerCollection
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        // handle index page
        $controllers->get('/', function (Application $app) {
            /** @var GeoClient $geo */
            $geo = $app['geo_client'];

            return $app['twig']->render('index.twig', [
                'client_ip' => $geo->getClientIp(),
            ]);
        });

        // handle request for geo location info
        $controllers->get('/location', function (Application $app) {
            /** @var GeoClient $geo */
            $geo = $app['geo_client'];

            try {
                return $app->json($geo->getLocationInfo());
            } catch (ClientLocationException $err) {
                return $app
                    ->json([
                        'error' => $err->getMessage(),
                        'error_trace' => $app['debug'] ? $err->getTraceAsString() : null
                    ])
                    ->setStatusCode(422);
            }
        });

        return $controllers;
    }
}
