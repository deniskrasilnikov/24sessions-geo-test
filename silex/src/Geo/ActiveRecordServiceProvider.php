<?php declare(strict_types=1);

namespace Geo;

use Pimple\ServiceProviderInterface;
use Pimple\Container;
use ActiveRecord\Config as ARConfig;
use Silex\Api\BootableProviderInterface;
use Silex\Application;

/**
 * Register PHP Active Record service
 */
class ActiveRecordServiceProvider implements ServiceProviderInterface, BootableProviderInterface
{
    /**
     * {@inheritdoc}
     *
     * @param Container $app
     */
    function register(Container $app)
    {
        $app['ar.init'] = function ($app) {
            ARConfig::initialize(function (ARConfig $cfg) use ($app) {
                $cfg->set_model_directory($app['ar.model_dir']);
                $cfg->set_connections($app['ar.connections']);
                $cfg->set_default_connection($app['ar.default_connection'] ?? 'default');
            });
        };
    }

    /**
     * {@inheritdoc}
     *
     * @param Application $app
     */
    public function boot(Application $app)
    {
        $app['ar.init'];
    }
}
