<?php declare(strict_types=1);

namespace Geo;

use Geo\Exception\ClientLocationException;
use Geo\Exception\LocationProviderException;
use Geo\Model\ClientLocation;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use ActiveRecord\Validations;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service encapsulates all geo-location retrieval related logic
 */
class ClientLocationService
{
    /**
     * Whether development mode is enabled
     *
     * @var bool
     */
    private $isDevMode;

    /**
     * Time-to-live for the location info recorded for a particular client ip
     *
     * @var int
     */
    private $locationTtl;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var LocationProviderInterface
     */
    private $locationProvider;

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var Request
     */
    private $request;

    /**
     * ClientLocationService constructor.
     *
     * @param ClientInterface $httpClient
     * @param Request $request
     * @param LocationProviderInterface $locationProvider
     * @param LoggerInterface $logger
     * @param bool $isDevMode Whether development mode is enabled
     * @param int $locationTtl Zero TTL means that location is cached for infinite
     */
    public function __construct(
        ClientInterface $httpClient,
        Request $request,
        LocationProviderInterface $locationProvider,
        LoggerInterface $logger,
        bool $isDevMode = false,
        int $locationTtl = 0
    )
    {
        $this->httpClient = $httpClient;
        $this->request = $request;
        $this->locationProvider = $locationProvider;
        $this->logger = $logger;
        $this->isDevMode = $isDevMode;
        $this->locationTtl = $locationTtl;
    }

    /**
     * Returns client`s IP address
     *
     * @return string|null
     *
     * @throws ClientLocationException
     */
    public function getClientIp(): ?string
    {
        // in local development environment we can not rely on REMOTE_ADDR
        // so falling back to external IP retrieval service
        if ($this->isDevMode) {
            try {
                // not much sense for moving this URL into external config param because response parsing logic
                // fully depends on this particular one so changing remote service will mean changing code anyway
                $response = $this->httpClient->request('GET', 'http://checkip.dyndns.com');
            } catch (GuzzleException $err) {
                $this->logger->error($err);

                throw new ClientLocationException('Error on determining client public ip. Check log for details.', $err);
            }

            preg_match(
                '/(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/',
                (string)$response->getBody(),
                $ip
            );

            return $ip[0] ?? null;
        }

        // NOTE: not a 100% guarantee to be real client IP but kept simple for this project
        return filter_var($this->request->server->get('REMOTE_ADDR'), FILTER_VALIDATE_IP) ?: null;
    }

    /**
     * Return client`s geo location information (city, country, other)
     *
     * @return array
     *
     * @throws ClientLocationException
     */
    public function getLocationInfo(): array
    {
        $clientIpAsInt = ip2long($clientIp = $this->getClientIp());

        /** @var ClientLocation $location */
        $location = ClientLocation::find(['ip' => $clientIpAsInt]);

        if (!is_object($location)) {
            $location = $this->createLocation();
            $location->ip = $clientIpAsInt;
        }

        if (!$location->isValid($this->locationTtl)) {
            // refreshing and persisting new location info
            try {
                $locationInfo = $this->locationProvider->getLocationInfo($clientIp);
            } catch (LocationProviderException $err) {
                $this->logger->error($err);

                throw new ClientLocationException('Error on getting info from location provider. Check log for details.', $err);
            }

            $location->country = $locationInfo->getCountry();
            $location->city = $locationInfo->getCity();
            $location->save();

            if (!$location->errors->is_empty()) {
                $this->logger->error(
                    $error = sprintf(
                        'Invalid model data %s: %s',
                        json_encode($location->to_array()),
                        implode(', ', $location->errors->full_messages())
                    )
                );
                throw new ClientLocationException($error);
            }
        }

        return $location->to_array();
    }

    /**
     * @return ClientLocation
     */
    public function createLocation(): ClientLocation
    {
        return new ClientLocation;
    }
}
