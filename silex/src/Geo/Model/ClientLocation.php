<?php declare(strict_types=1);

namespace Geo\Model;

use ActiveRecord;

/**
 * Entity representing client`s geo-location information in relation to IP address
 *
 * @package Geo
 */
class ClientLocation extends ActiveRecord\Model
{
    static $validates_numericality_of = [
        ['ip', 'greater_than' => 0]
    ];

    static $validates_presence_of = [['city'], ['country']];

    /**
     * If client location info is still valid by time (not outdated)
     *
     * @param int $ttl Time-to-live for the location info, in seconds
     *
     * @return bool
     */
    public function isValid(int $ttl): bool
    {
        if (!$this->updated_at instanceof \DateTime || !$this->ip) {
            return false;
        }

        if (!$ttl) {
            return true;
        }

        return (clone $this->updated_at)->modify("+$ttl seconds") > new \DateTime;
    }
}
