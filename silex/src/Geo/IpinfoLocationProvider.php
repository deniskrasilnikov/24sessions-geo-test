<?php declare(strict_types=1);

namespace Geo;

use Geo\Exception\LocationProviderException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Intl\Intl;

/**
 * Provides geo location info from ipinfo.io service
 *
 * @package Geo
 */
class IpinfoLocationProvider implements LocationProviderInterface
{
    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * Auth token for accessing ipinfo.io API (https://ipinfo.io/developers)
     *
     * @var string
     */
    private $apiToken;

    /**
     * IpinfoLocationProvider constructor.
     *
     * @param ClientInterface $httpClient
     * @param string $apiToken
     */
    public function __construct(ClientInterface $httpClient, string $apiToken)
    {
        $this->httpClient = $httpClient;
        $this->apiToken = $apiToken;
    }

    /**
     * {@inheritdoc}
     *
     * @throws LocationProviderException
     */
    public function getLocationInfo(string $ip): LocationInfo
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                sprintf('http://ipinfo.io/%s/geo?token=%s', $ip, $this->apiToken)
            );
        } catch (GuzzleException $err) {
            throw new LocationProviderException('Error on requesting geo location info', $err);
        }

        if ($response->getStatusCode() !== 200) {
            throw new LocationProviderException(
                sprintf(
                    'Bad response (code %d) from geo location service: %s',
                    $response->getStatusCode(),
                    (string)$response->getBody()
                )
            );
        }

        $geoInfo = json_decode((string)$response->getBody());

        if (!$geoInfo) {
            throw new LocationProviderException('Error on getting valid json response: '. $response->getBody());
        }

        return new LocationInfo(
            $geoInfo->city,
            Intl::getRegionBundle()->getCountryName($geoInfo->country)
        );
    }
}
