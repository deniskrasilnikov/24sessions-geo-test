<?php declare(strict_types=1);

namespace Geo;

/**
 * Location info data transfer object
 *
 * @package Geo
 */
class LocationInfo
{
    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $city;

    /**
     * LocationInfo constructor.
     * @param string $city
     * @param string $country
     */
    public function __construct(string $city, string $country)
    {
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * Get country name
     *
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * Get city name
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }
}
