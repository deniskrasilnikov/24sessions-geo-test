<?php

namespace Geo;

/**
 * Any third-party service which provides geo-location info must implement this.
 * @package Geo
 */
interface LocationProviderInterface
{
    /**
     * Provide location info for the given IP address from third-party geo service
     *
     * @param string $ip
     *
     * @return LocationInfo
     */
    public function getLocationInfo(string $ip): LocationInfo;
}
