<?php

namespace Geo\Exception;

/**
 * Exception occured at the level of any concrete Geo\LocationProviderInterface implementation
 *
 * @package Geo\Exception
 */
class LocationProviderException extends \RuntimeException
{
    /**
     * LocationProviderException constructor.
     * @param string $message
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = "", \Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);
    }
}
