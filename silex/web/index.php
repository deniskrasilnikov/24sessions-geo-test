<?php

use Geo\ClientLocationService;
use Geo\ControllerProvider;
use Geo\ActiveRecordServiceProvider;
use Geo\IpinfoLocationProvider;
use GuzzleHttp\Client as HttpClient;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application;
$app['debug'] = (bool)getenv('APP_DEV_PERMITTED');
$app['ar.model_dir'] = __DIR__ . '/../src/Geo/Model';
$app['ar.connections'] = [
    'default' => sprintf('mysql://%s:%s@db/%s', getenv('DB_USER'), getenv('DB_PASSWORD'), getenv('DB_NAME'))
];

// fix ActiveRecord issue with invalid default datetime format for MySQL
ActiveRecord\Connection::$datetime_format = 'db';

// wrapped some services registration into dedicated providers for demonstration
// and in order to reduce complexity of this front controller file

// PHP Active Record
$app->register(new ActiveRecordServiceProvider);

// Twig templating
$app->register(new Silex\Provider\TwigServiceProvider, [
    'twig.path' => __DIR__ . '/../src/views',
]);

// logging
$app->register(new Silex\Provider\MonologServiceProvider, [
    'monolog.logfile' => __DIR__ . '/../var/dev.log',
]);

$app['location_provider.ipinfo'] = function () {
    return new IpinfoLocationProvider(
        new HttpClient,
        getenv('IPINFO_TOKEN')
    );
};

// custom Geo location service
$app['geo_client'] = function ($app) {
    return new ClientLocationService(
        new HttpClient,
        $app['request_stack']->getCurrentRequest(),
        $app['location_provider.ipinfo'],
        $app['logger'],
        $app['debug'],
        (int)getenv('GEO_LOCATION_CACHE_TTL')
    );
};

$app->mount('/', new ControllerProvider);

$app->run();
