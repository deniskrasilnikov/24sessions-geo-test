(function () {
    var $geoResponse = $('#geoResponse');

    $("#getGeoLocation").click(function () {
        $.getJSON("/location", function (data) {
            $geoResponse.html('Your current location is ' + data.city + ', ' + data.country);
        }).fail(function () {
            $geoResponse.html('Service is unavailable.');
        });
    });
})();


