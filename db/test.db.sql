CREATE TABLE client_locations (
  ip      INT UNSIGNED NOT NULL
  COMMENT 'Using IP as a natural PK as we don\'t assume duplicates by business requirements',
  country VARCHAR(500),
  city    VARCHAR(500),
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Time of last location refresh',
  PRIMARY KEY (ip)
)
  ENGINE = InnoDB
  CHARACTER SET = utf8;
