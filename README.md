Geo-location service for 24sessions test assignment 
==============

This is a ready-to-launch local development environment with a test assignment completed according to next requirements:

> Develop geolocation service based on user IP address.

> Frontend contains one page which shows the IP address of the current user and button "Get geolocation". When the user clicks the button, we load location info (city and country) via Ajax request to the backend.

> Backend retrieves information from the JSON service of ipinfo.io with caching into the database. Only city and country must be cached and returned to Frontend. Let's assume that we have MySQL database "test" on localhost with user "test_user" and with password "secret"

> The backend should be built on Silex (latest version) with ActiveRecord ORM (https://packagist.org/packages/php-activerecord/php-activerecord) using Composer.

> For Frontend, jQuery can be used. 

# Project structure

* ./db - mysql Docker service configuration (including initial db structure dump)
* ./logs - nginx logs
* ./nginx - nginx Docker service configuration
* ./php-fpm - php Docker service configuration
* ./silex - application sources mounted into php and nginx containers

# Installation

First, clone this repository:

```bash
git clone git@bitbucket.org:deniskrasilnikov/24sessions-geo-test.git
```

Next, duplicate docker environment variables file `.env.dist` as `.env` and modify variables to your needs. 

Add `geo.localhost` host value for the 127.0.0.1 ip to the /etc/hosts file.

Then run:

```bash
docker-compose up -d
```

(Optionally) install vendor dependencies with:

```bash
docker-compose exec -T php composer install
```

Open service by `http://geo.localhost` address.

# Unit tests
You can run unit tests with `docker-compose exec php vendor/bin/phpunit` command.
